# php-laravel-5.4

#Criando Projeto 
composer create-project --prefer-dist laravel/laravel blog "5.4.*"

#Rodar projeto
php artisan serve

#publicar vendor
php artisan vendor:publish

#Laravel 5 Repositories
#Installar
#https://github.com/andersao/l5-repository
# Ao final criar a pasta Entities no diretorio app
composer require prettus/l5-repository

#comando de criação
php artisan make

#comando de criação entity
php artisan make:entity Users


php artisan make:model UserSocial -m

#comando de criação de estrutura do banco de dados
php artisan migrate


#adicionando o componentes de form
composer require laravelcollective/html